'use strict';

var textSequenceModule = (function() {

    var textObject;
    var textArray = [];
    var displayTime;
    var currentIndex = 0;
    var targetIndex = 0;
    var nextSwitchTime = 0;

    var isFinished = false;
    var isInitiated = false;

    var currentAlpha = 0;
    var targetAlpha = 1;
    var ALPHADELTA = 0.050;

    var skipInfoObject;

    var exports = {
        reset: function() {
            isInitiated = false;
            isFinished = false;
            currentIndex = 0;
            currentAlpha = 0;
            targetIndex = 0;
            textArray = [];
        },
        initiate: function(currentTime, textObject_, textArray_, displayTime_) {

            textObject = textObject_;
            textArray = textArray_;
            displayTime = displayTime_;
            nextSwitchTime = currentTime + displayTime;
            targetAlpha = 1;
            isInitiated = true;
            isFinished = false;
            targetIndex = 0;
            currentIndex = 0;
            currentAlpha = 0;
        },
        initiateSkipMessage: function(textObject) {
            skipInfoObject = textObject;
        },
        update: function(currentTime) {

            if (isFinished || !isInitiated) {
                return;
            }

            updateText(currentTime);
            updateAlpha();
        },
        isFinished: function() {
            return isFinished;
        },
        setEarlyFinish: function() {
            isFinished = true;
            targetAlpha = 0;
        },
        isSetup: function() {
            return isInitiated;
        }
    };

    var updateText = function(currentTime) {
        if (currentTime >= nextSwitchTime) {
            targetIndex += 1;
            targetAlpha = 0;
            nextSwitchTime = currentTime + displayTime;
        }

        if (currentIndex >= textArray.length) {
            isFinished = true;
            targetAlpha = 0;
        }
        else {
            textObject.text = textArray[currentIndex];
            textObject.alpha = currentAlpha;
        }
    };

    var updateAlpha = function() {
        if (currentAlpha < targetAlpha) {
            currentAlpha += ALPHADELTA;
            if (currentAlpha >= targetAlpha) {
                currentAlpha = targetAlpha;
            }
            if (skipInfoObject.alpha < currentAlpha) {
                skipInfoObject.alpha = currentAlpha;
            }
        }
        else if (currentAlpha > targetAlpha) {
            currentAlpha -= ALPHADELTA;
            if (currentAlpha <= targetAlpha) {
                currentAlpha = targetAlpha;
                if (!isFinished) {
                    targetAlpha = 1;
                    currentIndex = targetIndex;
                }
            }
            if (displayingLast()) {
                skipInfoObject.alpha = currentAlpha;
            }
        }
    };

    var displayingLast = function() {
        return currentIndex === textArray.length - 1;
    };

    return exports;

}());