'use strict';

var sceneMod = (function(){

    var COLUMNWIDTH = 32;
    var UPPERBOUNDY = 20;
    var LOWERBOUNDY = 378;

    var isDefined = false;
    var nbrColumns;
    var totalWidth;
    var xPos;
    var gameHeight;
    var gameWidth;

    var exports = {
        setColumnCount: function(gameWidth_, gameHeight_, columns) {
            nbrColumns = columns;
            totalWidth = COLUMNWIDTH * columns;
            xPos = gameWidth_ / 2 - totalWidth / 2;
            gameHeight = gameHeight_;
            gameWidth = gameWidth_;

            isDefined = true;
        },
        updateColumnCount: function(columns) {
            nbrColumns = columns;
            totalWidth = COLUMNWIDTH * columns;
            xPos = gameWidth / 2 - totalWidth / 2;
        },
        getXForColumn: function(column) {
            if (!isDefined) { throw new Error("Needs to be defined before use!"); }

            if (column > nbrColumns || column < 1) {
                throw new Error("Given column index: " + column + " is out of range: 1 - " + nbrColumns);
            }

            return xPos + (column - 1) * COLUMNWIDTH;
        },
        getXLeftWall: function() {
            if (!isDefined) { throw new Error("Needs to be defined before use!"); }
            return xPos;
        },
        getXRightWall: function() {
            if (!isDefined) { throw new Error("Needs to be defined before use!"); }
            return xPos + totalWidth;
        },
        getMinColumn: function() {
            return 1;
        },
        getMaxColumn: function() {
            return nbrColumns;
        },
        getGameHeight: function() {
            return gameHeight;
        },
        getBottomCoords: function() {
            var bottomCoords = [];
            for (var col = 0; col < nbrColumns; col++) {
                bottomCoords.push([xPos + col*COLUMNWIDTH, LOWERBOUNDY]);
            }
            return bottomCoords;
        },
        getTopCoords: function() {
            var topCoords = [];
            for (var col = 0; col < nbrColumns; col++) {
                topCoords.push([xPos + col*COLUMNWIDTH, UPPERBOUNDY]);
            }
            return topCoords;
        },
        getUpperBound: function() {
            return UPPERBOUNDY;
        },
        getLowerBound: function() {
            return LOWERBOUNDY;
        },
        getColumnWidth: function() {
            return COLUMNWIDTH;
        }
    };

    return exports;
}());