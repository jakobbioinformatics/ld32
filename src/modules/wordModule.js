'use strict';

var wordModule = (function(){

    var BASECOLOR = '#cccccc';
    var HIGHLIGHTCOLOR = '#00ff00';
    var TARGETLETTERCOLOR = '#ff0000';

    var lexicon;
    //var startTime = 0;
    var targetPos = 0;
    var currentWord;
    var that;

    var exports = {
        setupLexicon: function(scope, fileKey) {

            targetPos = 0;
            that = scope;
            lexicon = createLexicon(fileKey);
            currentWord = getRandomWord();
            that.game.input.keyboard.onDownCallback = keyInput.bind(that);
        },
        assignTextAndColor: function(textObject) {
            textObject.text = currentWord;
            textObject.clearColors();
            textObject.addColor(TARGETLETTERCOLOR, targetPos);
            textObject.addColor(BASECOLOR, targetPos + 1);
        },
        isWordFinished: function() {
            return targetPos >= currentWord.length;
        },
        reset: function() {
            if (!this.isWordFinished()) {
                return;
            }

            targetPos = 0;
            currentWord = getRandomWord();
        }
    };

    var createLexicon = function(filekey) {
        var rawText = that.game.cache.getText(filekey);
        var lines = rawText.split('\n');
        for (var n = 0; n < lines.length; n++) {
            var target = lines[n];
            target = target.replace('\r', '');  // Attempt to fix Windows return issues
            lines[n] = target.substring(0, target.length);
        }
        return lines;
    };

    var keyInput = function() {
        var keyCode = this.game.input.keyboard.event.keyCode;
        var clickedChar = String.fromCharCode(keyCode);

        if (clickedChar.toLowerCase() == getTargetChar()) {
            targetPos++;
        }
    };

    var getTargetChar = function() {
        return currentWord[targetPos];
    };

    var getRandomWord = function() {
        return lexicon[Math.floor(Math.random() * lexicon.length)];
    };

    return exports;
}());

