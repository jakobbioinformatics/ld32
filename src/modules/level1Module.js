'use strict';

var level1Module = (function() {

    //var WAVEDURATION = 2000;
    //var WAVEDELAY = 500;
    var WAVEDURATION = 20000;
    var WAVEDELAY = 5000;
    var WAVECOUNT = 5;

    var waveEndTime;
    var waveStartTime;
    var currentWave = 1;
    var nextWaveStart = 0;
    var isWaveStarted = false;
    var currentTime = 0;
    var isGameLost = false;

    var exports = {
        initiate: function(currentTime_) {
            waveEndTime = currentTime_ + WAVEDURATION;
            isWaveStarted = true;
            waveStartTime = currentTime_;
            currentTime = currentTime_;
            currentWave = 1;
            nextWaveStart = 0;
            isGameLost = false;
        },
        setWave: function(wave) {
            currentWave = wave;
        },
        getWave: function() {
            return currentWave;
        },
        setGameOver: function() {
            isGameLost = true;
        },
        getCurrentStatus: function() {

            if (isGameLost) {
                return 'lost';
            }

            if (currentWave > WAVECOUNT) {
                return 'finished';
            }

            if (!isWaveStarted) {
                return 'pause';
            }
            else {
                return 'running';
            }
        },
        getStatusString: function() {

            if (this.getCurrentStatus() === 'running') {
                return "Wave " + this.getWave()
                    + " " + this.getCurrentStatus()
                    + " " + getTimeString() + " seconds";
            }
            else if (this.getCurrentStatus() === 'pause') {
                return "Wave " + this.getWave()
                    + " starts in " + timeToNextSeconds() + " seconds..";
            }
            else {
                return "";
            }

        },
        update: function(currentTime_) {
            //var elapsed = elapsedSeconds(currentTime);
            if (!isWaveStarted && currentTime_ > nextWaveStart) {
                waveEndTime = currentTime_ + WAVEDURATION;
                isWaveStarted = true;
                waveStartTime = currentTime_;
            }
            else if (isWaveStarted) {
                if (currentTime_ >= waveEndTime) {
                    nextWaveStart = currentTime + WAVEDELAY;
                    isWaveStarted = false;
                    currentWave += 1;
                }
            }
            currentTime = currentTime_;
        },
        getColumns: function(currentTime) {

            switch (currentWave) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                default:
                    return 5;
            }
        },
        getSpawnDelay: function(currentTime) {

            if (this.getCurrentStatus() === 'pause') {
                return 1000000000;
            }

            switch (LD32Game.difficulty) {
                case 'Novice':
                    switch (currentWave) {
                        case 1:
                            return 5000;
                        case 2:
                            return 4500;
                        case 3:
                            return 4000;
                        case 4:
                            return 3500;
                        case 5:
                            return 3000;
                        default:
                            return 3000;
                    }
                    break;
                case 'Intermediate':
                    switch (currentWave) {
                        case 1:
                            return 3000;
                        case 2:
                            return 2500;
                        case 3:
                            return 2000;
                        case 4:
                            return 1500;
                        case 5:
                            return 1500;
                        default:
                            return 1500;
                    }
                    break;
                case 'Expert':
                    switch (currentWave) {
                        case 1:
                            return 1500;
                        case 2:
                            return 1500;
                        case 3:
                            return 1200;
                        case 4:
                            return 1000;
                        case 5:
                            return 800;
                        default:
                            return 800;
                    }
                    break;
            }

        },
        getBackgroundTransparency: function(currentTime) {

            switch (currentWave) {
                case 1:
                    return 1;
                case 2:
                    return 0.8;
                case 3:
                    return 0.6;
                case 4:
                    return 0.5;
                case 5:
                    return 0.4;
                default:
                    return 0.6;
            }
        },
        getLexicon: function(currentTime) {

            switch (currentWave) {
                case 1:
                    return 'phase1';
                case 2:
                    return 'phase1';
                case 3:
                    return 'phase2';
                case 4:
                    return 'phase3';
                case 5:
                    return 'phase3';
                default:
                    return 'phase3';
            }
        }
    };

    var getTimeString = function() {
        return '' + elapsedSeconds(currentTime) + ' / ' + Math.floor(WAVEDURATION / 1000);
    };

    var elapsedSeconds = function() {
        return Math.floor((currentTime - waveStartTime) / 1000);
    };

    var timeToNextSeconds = function() {
        return ((WAVEDURATION + WAVEDELAY) / 1000) - Math.floor((currentTime - waveStartTime) / 1000);
    };

    return exports;

}());