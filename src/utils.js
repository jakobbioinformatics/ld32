'use strict';

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

var utilsModule = (function(){

    var exports = {
        groupCleanUp: function(group) {
            var aCleanup = [];
            group.forEachDead(function(item){
                aCleanup.push(item);
            });

            var i = aCleanup.length - 1;
            while(i > -1)
            {
                var getitem = aCleanup[i];
                getitem.destroy();
                i--;
            }
        },
        groupReset: function(group) {
            for (var n = 0; n < group.children.length; n++) {
                group.children[n].kill();
            }
            this.groupCleanUp(group);
        },
        colorCharacter: function(stringObj, characterPos, basecolor, color) {
            stringObj.addColor(basecolor, characterPos - 1);
            stringObj.addColor(color, characterPos);
            stringObj.addColor(basecolor, characterPos + 1);
        }
    };
    return exports;
}());

