'use strict';

var Background = function(game, x, y, spriteKey) {
    Phaser.Sprite.call(this, game, x, y, spriteKey);

    this.alpha = 1;
    this.targetAlpha = 1;

    this.ALPHADELTA = 0.005;
};

Background.prototype = Object.create(Phaser.Sprite.prototype);
Background.prototype.constructor = Background;

Background.prototype.update = function() {
    if (this.alpha < this.targetAlpha) {
        this.alpha += this.ALPHADELTA;
        if (this.alpha > this.targetAlpha) {
            this.alpha = this.targetAlpha;
        }
    }
    else if (this.alpha > this.targetAlpha) {
        this.alpha -= this.ALPHADELTA;
        if (this.alpha < this.targetAlpha) {
            this.alpha = this.targetAlpha;
        }
    }
};

Background.prototype.setAlpha = function(newAlpha) {
    this.targetAlpha = newAlpha;
};