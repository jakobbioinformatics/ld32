'use strict';

var Player = function(game, column, yPos, bulletGroup) {

    this.playerBullets = bulletGroup;
    this.laserSound = game.add.sound('shot');

    Phaser.Sprite.call(this, game, sceneMod.getXForColumn(column), yPos, 'brightBlob');

    this.animations.add('stage1', [0,1]);
    this.animations.add('stage2', [1,2]);
    this.animations.add('stage3', [2,3]);
    this.animations.play('stage1', 0.5, true);

    this.game.physics.arcade.enableBody(this);
    this.body.collideWorldBounds = true;
    this.anchor.x = 0.5;
    this.anchor.y = 0.5;

    this.column = column;

    this.leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.leftKey.direction = 'left';
    this.leftKey.onDown.add(this.movement, this);

    this.rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    this.rightKey.direction = 'right';
    this.rightKey.onDown.add(this.movement, this);

    //this.fireKey = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);
    //this.fireKey.onDown.add(this.fire, this);

    this.powerUpLevel = 0;
    this.powerUpThreshold = 10;

    this.nextPowerShotDelay = 0;
    this.remainingPowerShots = 0;
    this.POWERSHOTDELAY = 150;
    this.LETTERDAMAGE = 10;
};

Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

Player.prototype.update_player = function(currentTime) {

    this.currentTime = currentTime;

    if (this.remainingPowerShots > 0 && this.currentTime > this.nextPowerShotDelay) {
        this.firePowerup();
        this.remainingPowerShots -= 1;
        this.nextPowerShotDelay = this.currentTime + this.POWERSHOTDELAY;
    }

    this.body.position.x = sceneMod.getXForColumn(this.column);
};

Player.prototype.awardKill = function() {
    this.powerUpLevel += 1;
};

Player.prototype.fire = function(wordFinished, wordlength) {

    if (!this.alive || !wordFinished) {
        return;
    }

    if (this.powerUpLevel >= this.powerUpThreshold) {
        this.powerUpLevel = 0;
        this.remainingPowerShots = 5;

        return;
    }

    this.shootBullet(this.position.x, this.position.y - 20, 800, wordlength * this.LETTERDAMAGE);
};

Player.prototype.firePowerup = function() {

    for (var n = 0; n <= 0; n++) {
        this.shootBullet(this.position.x, this.position.y - 20, 400);
    }
};

Player.prototype.desperateFire = function() {

    var bottomCoords = sceneMod.getBottomCoords();
    var leftCoord = bottomCoords[0];
    var rightCoord = bottomCoords[bottomCoords.length - 1];

    var SHOTCOUNT = 50;

    for (var shot = 0; shot < SHOTCOUNT; shot++) {
        var xpos = leftCoord[0] + 10 + Math.random() * (rightCoord[0] - leftCoord[0] + 16);
        var ypos = leftCoord[1] + Math.random() * 150;

        this.shootBullet(xpos, ypos, 800, 500);
    }
};

Player.prototype.shootBullet = function(xpos, ypos, speed, damage) {

    var bulletDamage = 50;

    if (damage !== undefined) {
        bulletDamage = damage;
    }

    if (LD32Game.difficulty === 'Novice') {
        bulletDamage *= 1.4;
    }
    else if (LD32Game.difficulty === 'Expert') {
        bulletDamage *= 0.7;
    }

    var bullet = new Bullet(this.game, xpos, ypos, bulletDamage);
    bullet.body.velocity.y = -speed;
    bullet.anchor.x = 0.5;
    bullet.anchor.y = 0.5;
    this.playerBullets.add(bullet);

    this.laserSound.play();
};

Player.prototype.isPowerupReady = function() {
    return this.powerUpLevel >= this.powerUpThreshold;
};

Player.prototype.movement = function(key) {

    var minCol = sceneMod.getMinColumn();
    var maxCol = sceneMod.getMaxColumn();

    if (key.direction === 'left') {
        this.column -= 1;
        if (this.column < minCol) {
            this.column = minCol;
        }
    }
    else if (key.direction === 'right') {
        this.column += 1;
        if (this.column > maxCol) {
            this.column = maxCol;
        }
    }
};