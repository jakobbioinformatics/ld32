'use strict';

var Enemy = function(game, x, y) {

    Phaser.Sprite.call(this, game, x, y, 'darkBlob');
    this.hitSound = game.add.sound('sfx');

    this.VELOCITY = 30;
    //this.VELOCITY = 100;
    this.PUSHBACK = 30;

    this.animations.add('move', [0,1]);
    this.animations.add('hit', [2], 3, false);
    this.events.onAnimationComplete.add( function (instance) {
        instance.play('move', 1, true);
    }, this);
    this.animations.play('move', 1, true);

    this.outOfBoundsKill = true;
    this.checkWorldBounds = true;
    this.game.physics.arcade.enableBody(this);

    this.body.velocity.y = this.VELOCITY;
    this.health = 100;

    this.tempInvincibleDelay = 300;
    this.isInvincibleUntil = 0;

    this.gameHeight = sceneMod.getGameHeight();
};

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

Enemy.prototype.create = function() {
    this.animations.add('move');
};

Enemy.prototype.update_enemy = function(game) {

    if (this.hasEscaped()) {
        game.enemyEscape();
        this.kill();
    }
};

Enemy.prototype.hasEscaped = function() {
    return this.position.y > sceneMod.getLowerBound();
};

Enemy.prototype.isInvincible = function() {
    return this.game.time.now < this.isInvincibleUntil;
};

Enemy.prototype.inflictDamage = function(damage, player_reference) {

    if (this.isInvincible()) {
        return;
    }

    this.hitSound.play();

    this.game.plugins.screenShake.shake(5);

    this.health -= damage;
    this.animations.play('hit');
    if (this.health <= 0) {
        player_reference.awardKill();
        this.kill();
    }
    this.isInvincibleUntil = this.game.time.now + this.tempInvincibleDelay;

    if (this.position.y > this.PUSHBACK) {
        this.position.y -= this.PUSHBACK;
    }
    else {
        this.position.y = 0;
    }
};