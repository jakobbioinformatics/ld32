'use strict';

var Bullet = function(game, x, y, damage) {
    Phaser.Sprite.call(this, game, x, y, 'bullet');

    this.outOfBoundsKill = true;
    this.checkWorldBounds = true;
    this.game.physics.arcade.enableBody(this);
    this.body.velocity.y = 150;
    this.damage = damage;
};

Bullet.prototype = Object.create(Phaser.Sprite.prototype);
Bullet.prototype.constructor = Bullet;

Bullet.prototype.update = function() {

};
