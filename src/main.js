'use strict';

var LD32Game = LD32Game || {};

LD32Game.game = new Phaser.Game(320, 480, Phaser.AUTO, 'gameContainer');

LD32Game.game.state.add('boot', LD32Game.boot);
LD32Game.game.state.add('preload', LD32Game.preload);
LD32Game.game.state.add('menu', LD32Game.menu);
LD32Game.game.state.add('play', LD32Game.play);

LD32Game.game.state.start('boot');