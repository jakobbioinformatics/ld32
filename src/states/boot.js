'use strict';

var LD32Game = LD32Game || {};

LD32Game.boot = function() {};

LD32Game.boot.prototype = {
    preload: function() {
        this.load.image('preloadbar', 'assets/preload.png');
    },

    create: function() {
        this.game.stage.backgroundColor = '#000';
        this.state.start('preload');
    }
};