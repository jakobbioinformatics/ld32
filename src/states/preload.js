'use strict';

var LD32Game = LD32Game || {};

LD32Game.preload = function() {};

LD32Game.preload.prototype = {
    preload: function() {

        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadbar');
        this.preloadBar.anchor.setTo(0.5);
        this.load.setPreloadSprite(this.preloadBar);

        this.load.image('player', 'assets/player.png');
        this.load.image('enemy', 'assets/enemy.png');
        this.load.image('bullet', 'assets/bullet.png');
        this.load.image('wall', 'assets/wall.png');
        this.load.image('bottom', 'assets/bottom.png');

        this.load.image('brightestBackground', 'assets/backgrounds/brightestBackground.png');
        this.load.image('darkestBackground', 'assets/backgrounds/darkestBackground.png');
        this.load.image('grayBackground', 'assets/backgrounds/grayBackground.png');
        this.load.image('menuBackground', 'assets/backgrounds/background1.png');

        this.load.spritesheet('darkBlob', 'assets/darkBlob.png', 32, 32);
        this.load.spritesheet('brightBlob', 'assets/brightBlob.png', 32, 32);

        this.load.text('lex1', 'assets/lex1.txt');
        this.load.text('phase1', 'assets/lexicons/phase1.txt');
        this.load.text('phase2', 'assets/lexicons/phase2.txt');
        this.load.text('phase3', 'assets/lexicons/phase3.txt');

        this.game.load.audio('sfx', ['assets/sound/shortSoftBlip.wav']);
        this.game.load.audio('shot', ['assets/sound/shortLowPink.wav']);
        this.game.load.audio('awayout', 'assets/music/awayout_full.ogg');
        this.game.load.audio('awayout_menu', 'assets/music/awayout_menu.ogg');
    },

    create: function() {
        this.state.start('menu');
    }
};