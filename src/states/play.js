'use strict';

var LD32Game = LD32Game || {};

LD32Game.play = function() {};

LD32Game.play.prototype = {

    init: function() {

        console.log('starting play');

        this.TEXTDISPLAYTIME = 2000;

        this.game.plugins.screenShake = this.game.plugins.add(Phaser.Plugin.ScreenShake);

        this.backgroundGroup = this.add.group();

        this.textstyle = { font: "16px Monospace", fill: "#000", align: "center" };
        this.currentWord = this.game.add.text(this.game.width / 2, this.game.height - 80, "", this.textstyle);
        this.currentWord.anchor.x = 0.5;
        this.alertInfo = this.game.add.text(this.game.width / 2, this.game.height - 60, "", this.textstyle);
        this.alertInfo.anchor.x = 0.5;
        this.infoText = this.game.add.text(10, this.game.height - 30, "text", this.textstyle);

        this.hasEscaped = false;
        this.BOTTOMSPACING = 100;

        this.currentLevelModule = this.setupLevel();
        var COLUMNS = 1;
        sceneMod.setColumnCount(this.game.width, 400, COLUMNS);
        this.initiateBullets();
        this.setupEnemies();
        this.wallGroup = this.game.add.group();
        this.initiateWalls();

        this.player = new Player(this.game, Math.ceil(sceneMod.getMaxColumn() / 2),
            this.game.height - this.BOTTOMSPACING, this.bulletGroup);
        this.game.add.existing(this.player);

        //this.logKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        //this.logKey.onDown.add(this.outputObjectStatus, this);

        this.brightLayer = new Background(this.game, 0, 0, 'brightestBackground');
        this.middleLayer = new Background(this.game, 0, 0, 'grayBackground');
        this.darkLayer = new Background(this.game, 0, 0, 'darkestBackground');
        this.backgroundGroup.add(this.brightLayer);
        this.backgroundGroup.add(this.middleLayer);
        this.backgroundGroup.add(this.darkLayer);

        this.currentLexicon = this.currentLevelModule.getLexicon(this.game.time.now);
        wordModule.setupLexicon(this, this.currentLexicon);

        this.outroTextModule = Object.create(textSequenceModule);
        this.outroTextModule.reset();

        this.setupImportantMessage();

        this.song = this.game.add.audio('awayout');
        this.song.play('',0,0.5,true);
    },

    setupImportantMessage: function() {
        this.importantMessage = this.game.add.text(this.game.width / 2, this.game.height / 2, '', this.textstyle);
        this.importantMessage.alpha = 1;
        this.importantMessage.targetAlpha = 0;
        this.importantMessage.deltaAlpha = 0.01;
        this.importantMessage.anchor.set(0.5);
        this.importantMessage.text = 'Type to get it out';
    },

    setupLevel: function() {
        var levelModule = level1Module;
        levelModule.initiate(this.game.time.now);
        if (LD32Game.level === 'hard') {
            levelModule.setWave(3);
        }
        return levelModule;
    },

    initiateBullets: function() {
        this.bulletGroup = this.add.group();
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
    },

    initiateWalls: function() {

        for (var i = 0; i < this.wallGroup.children.length; i++) {
            this.wallGroup.children[i].kill();
        }

        utilsModule.groupCleanUp(this.wallGroup);

        this.wallGroup.create(sceneMod.getXLeftWall(), 20, 'wall');
        this.wallGroup.create(sceneMod.getXRightWall(), 20, 'wall');
        var bottomCoords = sceneMod.getBottomCoords();
        for (var n = 0; n < bottomCoords.length; n++) {
            this.wallGroup.create(bottomCoords[n][0], bottomCoords[n][1], 'bottom');
        }
        var topCoords = sceneMod.getTopCoords();
        for (var n = 0; n < topCoords.length; n++) {
            this.wallGroup.create(topCoords[n][0], topCoords[n][1], 'bottom');
        }
    },

    setupEnemies: function() {
        this.enemyGroup = this.add.group();
        this.enemyGroup.enableBody = true;
        this.enemyGroup.physicsBodyType = Phaser.Physics.ARCADE;
        this.nextSpawnAt = 0;
        this.spawnDelay = 1500;
    },

    update: function() {

        this.currentLevelModule.update(this.game.time.now);

        var status = this.currentLevelModule.getCurrentStatus();
        if (status === 'running') {
            this.spawnEnemy();
            this.spawnDelay = this.currentLevelModule.getSpawnDelay(this.game.time.now);
        }

        this.updateCollisions();
        this.updateWords(status);
        this.updateColumns(status);
        this.updateEnemies(status);
        this.updateBackground();

        this.player.update_player(this.game.time.now);

        this.infoText.clearColors();
        this.infoText.text = this.currentLevelModule.getStatusString();
        if (status === 'pause') {
            utilsModule.colorCharacter(this.infoText, 17, '#000', '#f00');
        }

        if (status === 'finished') {
            if (!this.outroTextModule.isSetup()) {
                this.setupOutroText();
            }
            else {
                this.outroTextModule.update(this.game.time.now);
            }

            if (this.outroTextModule.isFinished()) {
                this.song.stop();
                this.state.start('menu');
            }
        }

        if (status === 'lost') {
            if (!this.importantMessage.lostActivated) {

                this.importantMessage.clearColors();
                this.importantMessage.addColor('#fff', 0);
                this.importantMessage.text = '[Space] to return to menu';
                this.importantMessage.targetAlpha = 1;
                this.importantMessage.lostActivated = true;

                this.importantMessage.endDelay = this.game.time.now + 500;

            }

            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)
                && this.game.time.now > this.importantMessage.endDelay) {
                this.song.stop();
                this.state.start('menu');
            }
        }

        //if (status === 'lost') {
        //    this.game.state.start('menu');
        //}

        this.updateImportantMessage();
    },

    updateImportantMessage: function() {
        if (this.importantMessage.targetAlpha > 0) {
            this.importantMessage.alpha += this.importantMessage.deltaAlpha;
            if (this.importantMessage.alpha >= 1) {
                this.importantMessage.targetAlpha = 0;
            }
        }
        else if (this.importantMessage.alpha > 0) {
            this.importantMessage.alpha -= this.importantMessage.deltaAlpha;
            if (this.importantMessage.alpha <= 0) {
                this.importantMessage.alpha = 0;
                this.importantMessage.targetAlpha = 0;
            }
        }
    },

    updateEnemies: function(status) {

        if (status !== 'lost' && status !== 'finished') {
            for (var n = 0; n < this.enemyGroup.length; n++) {
                this.enemyGroup.children[n].update_enemy(this);
            }
        }
        else {
            utilsModule.groupReset(this.enemyGroup);
        }
    },

    updateCollisions: function() {
        this.physics.arcade.overlap(this.bulletGroup, this.enemyGroup, this.collideBullets, null, this);
        //this.physics.arcade.overlap(this.player, this.enemyGroup, this.playerHit, null, this);

        utilsModule.groupCleanUp(this.bulletGroup);
        utilsModule.groupCleanUp(this.enemyGroup);
    },

    updateWords: function(status) {

        if (status === 'finished' || status === 'lost') {
            this.currentWord.text = '';
            return;
        }

        wordModule.assignTextAndColor(this.currentWord);
        this.setAlertStatus();
        if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {

            var wordFinished = wordModule.isWordFinished();

            this.player.fire(wordFinished, this.currentWord.text.length);

            if (wordFinished) {
                wordModule.reset();

                var currLex = this.currentLevelModule.getLexicon(this.game.time.now);
                if (currLex !== this.currentLexicon) {
                    this.currentLexicon = currLex;
                    wordModule.setupLexicon(this, this.currentLexicon);
                }
            }
        }
    },

    updateColumns: function(status) {

        if (status !== 'finished' && status !== 'lost') {
            var columns = this.currentLevelModule.getColumns(this.game.time.now);
            if (columns !== sceneMod.getMaxColumn()) {
                if (columns === 2) {
                    this.importantMessage.text = 'Arrows to move';
                    this.importantMessage.targetAlpha = 1;
                }
                sceneMod.updateColumnCount(columns);
                this.initiateWalls();
            }
        }
        else {
            utilsModule.groupReset(this.wallGroup);
        }
    },

    updateBackground: function() {
        var currentMode = this.currentLevelModule.getCurrentStatus();
        if  (currentMode === 'finished' && this.enemyGroup.length === 0) {
            this.darkLayer.setAlpha(0);
            this.middleLayer.setAlpha(0);
        }
        else if (currentMode === 'lost') {
            this.darkLayer.setAlpha(1);
        }
        else {
            this.darkLayer.setAlpha(this.currentLevelModule.getBackgroundTransparency(this.game.time.now));
        }
    },

    setAlertStatus: function() {
        this.alertInfo.clearColors();
        if (wordModule.isWordFinished()) {
            if (this.player.isPowerupReady()) {
                this.alertInfo.text = "Space!";
                this.alertInfo.addColor('yellow', 0);
            }
            else {
                this.alertInfo.text = "Space!";
                this.alertInfo.addColor('white', 0);
            }
        }
        else {
            this.alertInfo.text = "";
            this.alertInfo.addColor('white', 0);
        }
    },

    enemyEscape: function() {
        if (!this.hasEscaped) {
            this.player.desperateFire();
            this.hasEscaped = true;
            this.importantMessage.targetAlpha = 1;
            this.importantMessage.text = 'Not yet..'
        }
        else {
            this.currentLevelModule.setGameOver();
            this.player.kill();
        }
    },

    collideBullets: function(bullet, enemy) {
        //bullet.kill();
        enemy.inflictDamage(bullet.damage, this.player);
    },

    playerHit: function(player, enemy) {
        player.kill();
    },

    spawnEnemy: function() {
        if (this.nextSpawnAt > this.time.now) {
            return;
        }

        this.nextSpawnAt = this.time.now + this.spawnDelay;
        var column = Math.ceil(Math.random() * sceneMod.getMaxColumn());
        var xpos = sceneMod.getXForColumn(column);
        var ypos = 0;
        var enemy = new Enemy(this.game, xpos, ypos);
        this.enemyGroup.add(enemy);
    },

    setupOutroText: function() {
        this.outroTextObject = this.game.add.text(this.game.width / 2, this.game.height / 2, '', this.textstyle);
        this.outroTextObject.anchor.set(0.5);
        var outroText = [];
        outroText.push('');
        outroText.push('I');
        outroText.push('Am');
        outroText.push('Free');
        outroText.push('');
        outroText.push('Thank you for playing');
        outroText.push('');
        outroText.push('');
        this.outroTextModule.initiate(this.game.time.now, this.outroTextObject, outroText, this.TEXTDISPLAYTIME);
    },

    outputObjectStatus: function() {

        var livingEnemies = this.enemyGroup.countLiving();
        var deadEnemies = this.enemyGroup.countDead();
        var livingBullets = this.bulletGroup.countLiving();
        var deadBullets = this.bulletGroup.countDead();

        var total = livingEnemies + deadEnemies + livingBullets + deadBullets;

        console.log("Living enemies: " + livingEnemies);
        console.log("Dead enemies: " + deadEnemies);
        console.log("Living bullets: " + livingEnemies);
        console.log("Dead bullets: " + deadEnemies);

        console.log("Total count: " + total);
    }
};



