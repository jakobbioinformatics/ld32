'use strict';

LD32Game.menu = function() {};

LD32Game.menu.prototype = {

    init: function() {
        this.game.add.sprite(0, 0, 'menuBackground');
        this.sfx = this.game.add.audio('sfx');
        this.menuSong = this.game.add.audio('awayout_menu');
    },

    create: function() {

        this.menuSong.play('', 0, 0.5, true);

        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.LEFT]);
        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.RIGHT]);

        this.TEXTDISPLAYTIME = 2000;

        this.currentTextAlpha = 1;
        this.ALPHADELTA = 0.05;
        this.fadeOutStarted = false;
        this.fadedOut = false;

        var yOffset = 70;

        this.textGroup = this.game.add.group();

        this.currentOption = 1;

        var header = "A Way Out";
        var text = "Press Spacebar to start game";
        var hardText = "Arrows to navigate";
        var typeText = "Keyboard keys to type";

        this.headerStyle = { font: "32px Monospace", fill: "#000", align: "center" };
        this.textStyle = { font: "14px Monospace", fill: "#000", align: "center"};

        var headerOut = this.game.add.text(this.game.width / 2 - 3, this.game.height / 2 - yOffset, header, this.headerStyle);
        var textOut = this.game.add.text(this.game.width / 2, this.game.height / 2 + yOffset, text, this.textStyle);
        var hardTextOut = this.game.add.text(this.game.width / 2, this.game.height / 2 + yOffset + 40, hardText, this.textStyle);
        var typeTextOut = this.game.add.text(this.game.width / 2, this.game.height / 2 + yOffset + 60, typeText, this.textStyle);

        this.textGroup.add(headerOut);
        this.textGroup.add(textOut);
        this.textGroup.add(hardTextOut);
        this.textGroup.add(typeTextOut);

        for (var n = 0; n < this.textGroup.length; n++) {
            this.textGroup.children[n].anchor.set(0.5);
        }

        this.setupChoiceText();
        this.setupKeys();

        this.introTextObject = this.game.add.text(this.game.width / 2, this.game.height / 2, '', this.textStyle);
        this.introTextObject.anchor.set(0.5);

        this.introTextModule = Object.create(textSequenceModule);
        this.introTextModule.reset();
    },

    setupChoiceText: function() {
        var centerOffset = 100;
        this.choiceTexts = [];
        var choice1text = this.game.add.text(this.game.width / 2 - centerOffset, this.game.height / 2, 'Novice', this.textStyle);
        var choice2text = this.game.add.text(this.game.width / 2, this.game.height / 2, 'Intermediate', this.textStyle);
        var choice3text = this.game.add.text(this.game.width / 2 + centerOffset, this.game.height / 2, 'Expert', this.textStyle);
        this.choiceTexts.push(choice1text);
        this.choiceTexts.push(choice2text);
        this.choiceTexts.push(choice3text);
        for (var n = 0; n < this.choiceTexts.length; n++) {
            this.choiceTexts[n].anchor.set(0.5);
        }

        this.textGroup.add(choice1text);
        this.textGroup.add(choice2text);
        this.textGroup.add(choice3text);
    },

    setupKeys: function() {
        this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        this.leftKey.direction = 'left';
        this.leftKey.onDown.add(this.optionMovement, this);

        this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        this.rightKey.direction = 'right';
        this.rightKey.onDown.add(this.optionMovement, this);
    },

    setupIntroText: function() {
        var introText = [];
        introText.push('My head..');
        introText.push('It is filled with all those thoughts');
        introText.push('Following me, staring at me');
        introText.push('I need to find a way out');
        introText.push('Come to terms with myself');
        introText.push('Writing is my weapon of choice');
        this.introTextModule.initiate(this.game.time.now, this.introTextObject, introText, this.TEXTDISPLAYTIME);
        var skipInfoOut = this.game.add.text(this.game.width / 2, this.game.height / 2 + 130, '[SPACE] to skip', this.textStyle);
        skipInfoOut.anchor.set(0.5);
        skipInfoOut.alpha = 0;
        this.introTextModule.initiateSkipMessage(skipInfoOut);
    },

    update: function() {

        if (!this.menuSong.isPlaying) {
            this.menuSong.play();
        }

        for (var n = 0; n < this.choiceTexts.length; n++) {
            this.choiceTexts[n].clearColors();
            if (n === this.currentOption) {
                this.choiceTexts[n].addColor('yellow', 0);
            }
        }

        if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
            LD32Game.difficulty = this.choiceTexts[this.currentOption].text;
            this.fadeOutStarted = true;
            //this.setupIntroText();

            if (this.introTextModule.isSetup() && !this.introTextModule.isFinished()) {
                this.introTextModule.setEarlyFinish();
            }
        }

        if (this.fadedOut && !this.introTextModule.isSetup()) {
            this.setupIntroText();
        }

        if (this.introTextModule.isFinished()) {
            this.menuSong.stop();
            this.game.state.start('play');
        }

        //textSequenceModule.update(this.game.time.now);
        this.introTextModule.update(this.game.time.now);

        if (this.fadeOutStarted) {
            this.updateTextAlpha();
        }
    },

    updateTextAlpha: function() {
        this.currentTextAlpha -= this.ALPHADELTA;
        if (this.currentTextAlpha <= 0) {
            this.currentTextAlpha = 0;
            this.fadedOut = true;
        }
        for (var n = 0; n < this.textGroup.length; n++) {
            this.textGroup.children[n].alpha = this.currentTextAlpha;
        }
    },

    optionMovement: function(key) {
        if (key.direction === 'left') {
            if (this.currentOption >= 1) {
                this.currentOption -= 1;
            }

            this.sfx.play();
        }
        if (key.direction === 'right') {
            if (this.currentOption <= 1) {
                this.currentOption += 1;
            }
            this.sfx.play();
        }
    }

};